﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryScript : MonoBehaviour
{
    public static int PhoneBattery;
    public Text Text;
    // Start is called before the first frame update
    void Start()
    {
        Text = gameObject.transform.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

        PhoneBattery = FindObjectOfType<PhoneBehaviour>().PhoneBattery;
        if (PhoneBattery!=0)
        {
            Text.GetComponent<Text>().text = ""+PhoneBattery;
        }
        else
        {
            
            GameManager._instance.UnlockAchievement("LightsOut");
            Text.GetComponent<Text>().text = "NO BATERRY";

        }
     
        
        
    }
}
