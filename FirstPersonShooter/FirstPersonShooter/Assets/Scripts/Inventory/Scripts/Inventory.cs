﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class Inventory  {

      public event EventHandler OnItemListChanged;
 
     private Item StartItem;
    [SerializeField]    int currentPosition=0;
    private List<Item> itemList;
   [SerializeField] private List<String> itemListIds;
    [SerializeField]  private Action<Item> useItemAction;
    public int GetItemPosition(Item item) {
        return itemList.IndexOf(item);
    }

    public void OnListChanged(object sender, System.EventArgs e)
    {
        itemListIds = new List<string>();
        foreach (var t in itemList)
        {
            itemListIds.Add(t.name);
        }
    }

    public void SetLoadedInventory(Inventory i)
    {
        currentPosition = i.currentPosition;
        LoadItemList(i.itemListIds);
        
    }

    public void LoadItemList(List<string> itemIdsList)
    {
        itemList = new List<Item>();
        foreach (var item in itemIdsList)
        {
            itemList.Add(Resources.Load<Item>("InventoryItems/"+ item));
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);

    }

    public Inventory(Action<Item> useItemAction) {
        this.useItemAction = useItemAction;
        itemList = new List<Item>();
        StartItem =Resources.Load<Item>("InventoryItems/Lantern");
     //   itemList.Add( StartItem);
        OnItemListChanged += OnListChanged;

    }
    public Item GetCurrentItem() {
        return itemList[currentPosition];
    }
    public void AddItem(Item item) {
        Debug.Log("item" + item.ToString() );

        if (item.IsStackable()) {
            bool itemAlreadyInInventory = false;
            foreach (Item inventoryItem in itemList) {
                //todo Make generic itemtype
                if (inventoryItem.itemType == item.itemType) {
                    inventoryItem.amount += item.amount;
                    itemAlreadyInInventory = true;
                }
            }
            if (!itemAlreadyInInventory) {
                itemList.Add(item);
            }
        } else {
            itemList.Add(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveItem(Item item) {
        if (item.IsStackable()) {
            Item itemInInventory = null;
            foreach (Item inventoryItem in itemList) {
                if (inventoryItem.itemType == item.itemType) {
                    inventoryItem.amount -= item.amount;
                    itemInInventory = inventoryItem;
                }
            }
            if (itemInInventory != null && itemInInventory.amount <= 0) {
                itemList.Remove(itemInInventory);
            }
        } else {
            itemList.Remove(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
      

    }

    public void UseItem(Item item) {
        useItemAction(item);
    }
    public void UseCurrentItem() {
        useItemAction(itemList[currentPosition]);
    }

    public Transform NextItem()
    {
        Debug.Log("Next Item");
        if (currentPosition < itemList.Count - 1)
            currentPosition++;
        else
        {
            currentPosition = 0;
        }

        OnItemListChanged?.Invoke(this, EventArgs.Empty);
        return itemList[currentPosition].GetTransform();


    }

 

    public Transform PreviousItem()
    {

        Debug.Log("Previous Item");
        if (currentPosition > 0)
            currentPosition--;
        else { currentPosition = itemList.Count-1; }
       
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
        return itemList[currentPosition].GetTransform();


    }

    public List<Item> GetItemList() {
        return itemList;
    }

    public int GetCurrentPosition()
    {
        return currentPosition;
    }
}
