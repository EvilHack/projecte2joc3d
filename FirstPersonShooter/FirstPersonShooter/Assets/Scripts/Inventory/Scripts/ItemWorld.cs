﻿using System.Collections;
using System.Collections.Generic;
 using UnityEngine;
 using TMPro;
 
public class ItemWorld : MonoBehaviour,Iinteractuable
{
    public int amount;
    public AudioClip onPickSound;
    public static ItemWorld SpawnItemWorld(Vector3 position, Item item) {
        Debug.Log("Va a spawnera : " + item.ToString() +" que tiene un transform de " + item.GetTransform());

        Transform transform = Instantiate(item.GetTransform(), position, Quaternion.identity);
    Debug.Log("Transform instantiado: " + transform + "    iw: "  +transform.GetComponent<ItemWorld>()  );
    Debug.Log(transform.GetComponent<ItemWorld>() + "!!!!!!!");
    ItemWorld itemWorld = transform.GetComponent<ItemWorld>();
            Debug.Log("El transform tiene un itemworld de" + itemWorld   +" + +"   + transform.name);
        itemWorld.SetItem(item);

        return itemWorld;
    }

    public static ItemWorld DropItem(Vector3 dropPosition, Item item) {
        Vector3 randomDir = new Vector3(UnityEngine.Random.Range(-1f,1f), UnityEngine.Random.Range(-1f,1f)).normalized;
        ItemWorld itemWorld = SpawnItemWorld(dropPosition + randomDir * 8f, item);
        itemWorld.GetComponent<Rigidbody2D>().AddForce(randomDir * 40f, ForceMode2D.Impulse);
        return itemWorld;
    }


    [SerializeField]private Item item;
    private SpriteRenderer spriteRenderer;
  //  private Light light2D;
    private TextMeshPro textMeshPro;

    private void Awake() {
     //   spriteRenderer = GetComponent<SpriteRenderer>();
    //    light2D = transform.Find("Light").GetComponent<Light>();
      //  textMeshPro = transform.Find("Text").GetComponent<TextMeshPro>();
    }

    public void SetItem(Item item) {
        Debug.LogError("Item is " + item);
        Debug.LogWarning("Item is " + item);
        Debug.Log("Item is " + item);
        this.item = item;
        //spriteRenderer.sprite = item.GetSprite();
      //  light2D.color = item.GetColor();
        if (item.amount > 1) {
            textMeshPro.SetText(item.amount.ToString());
        } else {
            textMeshPro.SetText("");
        }
    }

    public Item GetItem() {
        return item;
    }

    public void DestroySelf() {
        Destroy(gameObject);
    }

    public void onInteract(Collider other)
    {
     
        Item itemWithAmount = item;
        if (item.name=="Coin")
        {
            
            GameManager._instance.UnlockAchievement("FirstCoin");
        }else if (item.name=="Key")
        {
      
            GameManager._instance.UnlockAchievement("YouHaveTheKey");

        }
        if (this.amount!=0)
        {
            itemWithAmount.amount = amount;
        }
        SoundManager._instance.PlaySE(onPickSound,1);
        other.GetComponent<FPOverwrite>()?.AddItemToInventory(itemWithAmount);
        Debug.Log("Recolectado");
        DestroySelf();

    }

    public void onStopInteract(Collider other)
    {
       
    }
}
