﻿using System.Collections;
using System.Collections.Generic;
using CodeMonkey.Utils;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_Inventory : MonoBehaviour
{

    private Inventory inventory;
    private Transform itemSlotContainer;
  [SerializeField]  private Transform itemSlotTemplate;

    [SerializeField] private Transform selector;
    private FPOverwrite player;

    private void Awake()
    {
        itemSlotContainer = transform.Find("itemSlotContainer");
      //  itemSlotTemplate = itemSlotContainer.Find("itemSlotTemplate");
        Debug.Log("ItemSlotContainer " + itemSlotContainer);
        selector = Instantiate(selector, itemSlotContainer.parent);
        selector.gameObject.SetActive(false);

    }

    public void SetPlayer(FPOverwrite player)
    {
        this.player = player;
    }

    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;

        inventory.OnItemListChanged += Inventory_OnItemListChanged;

        RefreshInventoryItems();
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }
    List<Vector3> itemPositions = new List<Vector3>();

    private void RefreshInventoryItems()
    {
        int currentItemPos = 0;
        for (int i = 0; i < itemSlotContainer.childCount; i++)
        {
            if (itemSlotContainer.GetChild(i) == itemSlotTemplate) continue;
            Destroy(itemSlotContainer.GetChild(i).gameObject);
        }

        //   Item currentItem= inventory.GetCurrentItem();

        int x = 0;
        int y = 0;
        float itemSlotCellSize = 75f;
        if (inventory.GetItemList().Count!=0)
        {
            selector.gameObject.SetActive(true);
        }
        foreach (Item item in inventory.GetItemList())
        {

            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);
            //   selector.position = itemSlotTemplate.position;

            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("Hago cosas");
                inventory.RemoveItem(item);

            }
            itemSlotRectTransform.GetComponent<Button_UI>().ClickFunc = () =>
            {
                // Use item
                inventory.UseItem(item);
            };
            itemSlotRectTransform.GetComponent<Button_UI>().MouseRightClickFunc = () =>
            {
                // Drop item
                Item duplicateItem = new Item { itemType = item.itemType, amount = item.amount };
                inventory.RemoveItem(item);
                ItemWorld.DropItem(player.transform.position, duplicateItem);
            };

            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, -y * itemSlotCellSize);
            Image image = itemSlotRectTransform.Find("image").GetComponent<Image>();
            image.sprite = item.GetSprite();
            if (!itemPositions.Contains(itemSlotRectTransform.anchoredPosition))
                itemPositions.Add(itemSlotRectTransform.anchoredPosition);
            TextMeshProUGUI uiText = itemSlotRectTransform.Find("amountText").GetComponent<TextMeshProUGUI>();
            if (item.amount > 1)
            {
                Debug.Log("Item " + item.name + " has " + item.amount +" amount");
                uiText.SetText(item.amount.ToString());
            }
            else
            {
                uiText.SetText("");
            }
            if (currentItemPos == inventory.GetCurrentPosition())
            {
                Debug.Log("Current item pos" + currentItemPos + " and inv.curretnpos" + currentItemPos);
                selector.position = itemSlotRectTransform.position;

            }
            x++;
            if (x >= 4)
            {
                x = 0;
                y++;
            }
            currentItemPos++;
        }
    }


}
