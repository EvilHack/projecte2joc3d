﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
[CreateAssetMenu(fileName = "New Item", menuName = "new Item")]
public class Item : ScriptableObject
{
    [SerializeField] private Sprite sprite;
    [SerializeField] private Transform transform;
    [SerializeField] private Color color;
    [SerializeField] private bool isStackeable;
 
    private void Awake()
    {
     }

    public enum ItemType
    {
        
        Medkit,
        Coin,
        Lantern,
        Battery,
    }

    public ItemType itemType;
    public int amount;


    public Sprite GetSprite()
    {
      return  this.sprite;
    }

    public Transform GetTransform()
    {
        return transform;
    }


    public Color GetColor()
    {
     return   this.color;
    }

    public bool IsStackable()
    {return isStackeable;
    }

    public void Init(ItemType type, int am)
    {
        itemType = type;
        amount = am;
         
    }

    public override string ToString()
    {
        return itemType + " " + amount;
    }
}