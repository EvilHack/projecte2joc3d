﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charger : MonoBehaviour,Iinteractuable
{
    private bool isInteracting;
    public void onInteract(Collider other)
    {
        isInteracting = true;
GameObject phone=        GameObject.FindGameObjectWithTag("Mobile");
        if (phone != null)
        {
            Debug.Log("Empieza a cargar");
            GameManager._instance.UnlockAchievement("Charging Up");
            StartCoroutine(nameof(IncreaseBattery));
            GameManager._instance.SaveIntoJson();
        }
       /// isInteracting = false;
    }

    public void onStopInteract(Collider other)
    {
        isInteracting = false;
        GetComponentInChildren<ParticleSystem>().loop=false; 
       
        GetComponentInChildren<ParticleSystem>().enableEmission=false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator IncreaseBattery()
    {
        PhoneBehaviour phoneBehaviour = FindObjectOfType<PhoneBehaviour>();
        GetComponentInChildren<ParticleSystem>().loop = true;
        GetComponentInChildren<ParticleSystem>().enableEmission=true;

        while (isInteracting)
        {
            if (phoneBehaviour.PhoneBattery!=100)
            {
                phoneBehaviour.PhoneBattery+=10;
                Debug.Log("Cargando" + phoneBehaviour.PhoneBattery + "%");
                if (phoneBehaviour.PhoneBattery>=100)
                {
                    phoneBehaviour.PhoneBattery = 100;
                    Debug.Log("Bateria maxima");
                    GetComponentInChildren<ParticleSystem>().loop=false; 
                    
                    GetComponentInChildren<ParticleSystem>().enableEmission=false;

                    isInteracting = false;
                }                
                
            }
            GetComponentInChildren<ParticleSystem>().Play();

            yield return new  WaitForSeconds(1);
        }
    }
}
