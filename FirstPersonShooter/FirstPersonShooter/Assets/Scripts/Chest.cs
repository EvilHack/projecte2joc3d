﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour , Iinteractuable
{
    private Animation animation;
    public AudioClip openSound;
    private bool isOpened = false;
    public Item itemToGiveOnOpen;
    public int amount;
    // Start is called before the first frame update
    void Start()
    {
        animation = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onInteract(Collider other)
    {
        Debug.Log("Interactuo con cofre");
        if (!isOpened)
        {
            OpenChest();
            Item itemWithAmount = itemToGiveOnOpen;
            if (this.amount != 0)
            {
                itemWithAmount.amount = amount;
            }

            other.GetComponent<FPOverwrite>().AddItemToInventory(itemWithAmount);
            isOpened = true;
        }
        
    }

    public void onStopInteract(Collider other)
    {
         
    }

    public void OpenChest()
    {
        animation.Play();
            SoundManager._instance.PlaySE(openSound,1);
        Debug.Log("Cofre abierto");
    }
}
