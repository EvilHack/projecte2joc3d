﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyInLoad : MonoBehaviour
{
    public  static DontDestroyInLoad _instance;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (DontDestroyInLoad._instance == null)
        {
            _instance = this;
           
        }
        else
        {
            Destroy(this);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
