﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Door : MonoBehaviour, Iinteractuable
{
    [SerializeField] private DoorType _doorType;

    [SerializeField] private float startY = 0f;
    [SerializeField] public float openSpeed = 100f;
    [SerializeField] public float targetAngle = 90f;
    public int id;
    [SerializeField]
    public AnimationCurve speedCurve = new AnimationCurve(new Keyframe(0, 0.3f),
        new Keyframe(0.1f, 0.3f), new Keyframe(0.5f, 1f), new Keyframe(0.9f, 0.3f), new Keyframe(1f, 0.3f));

    public Transform door;
    [SerializeField] AudioClip openSound;
    [SerializeField] AudioClip closeSound;
    [SerializeField] float playCloseSoundAtPercent = 0.9f;
    public bool isOpen = false;
    public bool isTesting = false;
    private bool isOpening = false;
    private bool isClosing = false;
    private bool playedCloseSound = false;
    private Vector3 startPos;
    [SerializeField] public bool isBackwards = false;

    [SerializeField] private Transform pivot;

    // Start is called before the first frame update
    void Start()
    {
        switch (_doorType)
        {
            case DoorType.Normal:
                {
                    startY = door.transform.localRotation.eulerAngles.x;
                    if (isTesting)
                        Debug.Log("start X: " + startY.ToString());
                    break;
                }
            case DoorType.Z:
                {
                    startY = door.transform.localRotation.eulerAngles.z;
                    if (isTesting)
                        Debug.Log("start Z: " + startY.ToString());
                    break;
                }
            case DoorType.Sliding:
                {
                    startY = door.transform.localRotation.eulerAngles.y;
                    if (isTesting)
                        Debug.Log("start Y: " + startY.ToString());
                    break;
                }
        }

        startPos = door.transform.localPosition;
        Utilities.doorEvent += ToggleDoor;
    }


    // Update is called once per frame
    void Update()
    {
    }

    public void OpenDoor()
    {
        StopDoorAnims();
        isOpening = true;

        if (!isBackwards)
            StartCoroutine("ForwardDoor2");
        else
            StartCoroutine("ReverseDoor2");
        //if (openSound != null)
        //todo passar sonidos al soundmanager 
        //audioSource.PlayOneShot(openSound);
        isOpen = true;
    }

    public void CloseDoor()
    {
        StopDoorAnims();
        playedCloseSound = false;
        isClosing = true;
        Debug.Log("isclosing " + isClosing);
        if (!isBackwards)
            StartCoroutine("ReverseDoor2");
        else
            StartCoroutine("ForwardDoor2");

        isOpen = false;
    }

    public void StopDoorAnims()
    {
        StopCoroutine(nameof(ForwardDoor2));
        StopCoroutine(nameof(ReverseDoor2));
    }

    public void TriggerDoor(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            Debug.Log("Open  or close the door with E");

            //do stuff

            ToggleDoor(id);

        }
    }





    public void ToggleDoor(int id)
    {
        if (isOpen)
        {
            Debug.Log("Door close");
            CloseDoor();
        }
        else
        {
            OpenDoor();
            Debug.Log("Door opened");
        }
    }

    private IEnumerator ForwardDoor2()
    {
        switch (_doorType)
        {
            case DoorType.Normal:

                while (door.transform.localRotation.eulerAngles.x < startY + targetAngle - 1f)
                {
                    float calcSpeed = CalcSpeedRamp2(door.transform.localRotation.eulerAngles.x, startY,
                        startY + targetAngle);
                    door.transform.RotateAround(pivot.transform.position, Vector3.right, calcSpeed * Time.deltaTime);
                    //if (isTesting)
                    //Debug.Log("Opening.. X: " + doorObj.transform.localRotation.eulerAngles.x.ToString("0.000"));
                    isOpening = true;
                    yield return null;
                }

                door.transform.localEulerAngles = new Vector3(targetAngle, door.transform.localEulerAngles.y,
                    door.transform.localEulerAngles.z);

                break;
            case DoorType.Sliding:
                while (door.transform.localRotation.eulerAngles.y < startY + targetAngle - 1f)
                {
                    float calcSpeed = CalcSpeedRamp2(door.transform.localRotation.eulerAngles.y, startY,
                        startY + targetAngle);
                    door.transform.RotateAround(pivot.transform.position, Vector3.left, calcSpeed * Time.deltaTime);
                    //if (isTesting)
                    //Debug.Log("Opening.. Y: " + doorObj.transform.localRotation.eulerAngles.y.ToString("0.000"));
                    isOpening = true;
                    yield return null;
                }

                door.transform.localEulerAngles = new Vector3(door.transform.localEulerAngles.x, targetAngle,
                    door.transform.localEulerAngles.z);

                break;
            case DoorType.Z:
                while (door.transform.localRotation.eulerAngles.y < startY + targetAngle - 1f)
                {
                    Debug.Log("º" + door.transform.localRotation.eulerAngles.y);

                    float calcSpeed = CalcSpeedRamp2(door.transform.localRotation.eulerAngles.y, startY,
                        startY + targetAngle);
                    door.transform.RotateAround(pivot.transform.position, Vector3.up, calcSpeed * Time.deltaTime);
                    //if (isTesting)
                    //Debug.Log("Opening.. Z: " + doorObj.transform.localRotation.eulerAngles.z.ToString("0.000"));
                    isOpening = true;
                    yield return null;
                }

                door.transform.localRotation = Quaternion.Euler(door.transform.localRotation.eulerAngles.x, targetAngle,
                    door.transform.localRotation.eulerAngles.z);

                break;
            case DoorType.Slidingv2:
                while (door.position != pivot.transform.position)
                {
                    float calcSpeed = CalcSpeedRamp2(door.transform.position.x, 0,
                         pivot.transform.position.x);

                    door.position = Vector3.MoveTowards(startPos, pivot.transform.position, Time.deltaTime * calcSpeed);



                    isOpening = true;
                    yield return null;
                }

                //  door.transform.localRotation = Quaternion.Euler(door.transform.localRotation.eulerAngles.x, targetAngle,
                //    door.transform.localRotation.eulerAngles.z);
                break;
        }


        isOpening = false;
    }


    private IEnumerator ReverseDoor2()
    {
        switch (_doorType)
        {
            case DoorType.Normal:

                while (door.transform.localRotation.eulerAngles.x > startY &&
                       door.transform.localRotation.eulerAngles.x < targetAngle + 1f)
                {
                    float calcSpeed = CalcSpeedRamp2(door.transform.localRotation.eulerAngles.x, startY,
                        startY + targetAngle);
                    door.transform.RotateAround(pivot.transform.position, Vector3.right,
                        -1 * calcSpeed * Time.deltaTime);
                    //if (isTesting)
                    //Debug.Log("Close door X: " + doorObj.transform.localRotation.eulerAngles.x.ToString("0.000"));
                    isClosing = true;
                    yield return null;
                }

                door.transform.localEulerAngles = new Vector3(startY, door.transform.localEulerAngles.y,
                    door.transform.localEulerAngles.z);
                door.transform.localPosition = startPos;
                break;
            case DoorType.Sliding:
                while (door.transform.localRotation.eulerAngles.y > startY &&
                       door.transform.localRotation.eulerAngles.y < targetAngle + 1f)
                {
                    float calcSpeed = CalcSpeedRamp2(door.transform.localRotation.eulerAngles.y, startY,
                        startY + targetAngle);
                    door.transform.RotateAround(pivot.transform.position, Vector3.left,
                        -1 * calcSpeed * Time.deltaTime);
                    //if (isTesting)
                    //Debug.Log("Close door Y: " + doorObj.transform.localRotation.eulerAngles.y.ToString("0.000"));
                    isClosing = true;
                    yield return null;
                }

                door.transform.eulerAngles = new Vector3(door.transform.rotation.x, startY, door.transform.rotation.z);
                door.transform.localPosition = startPos;
                break;
            case DoorType.Z:
                Debug.Log("con0" + (door.transform.localRotation.eulerAngles.y > startY &&
                                    door.transform.localRotation.eulerAngles.y <= targetAngle + 1f));

                while (door.transform.localRotation.eulerAngles.y > startY &&
                       door.transform.localRotation.eulerAngles.y <= targetAngle + 1f)
                {
                    Debug.Log("con" + (door.transform.localRotation.eulerAngles.y > startY &&
                                       door.transform.localRotation.eulerAngles.y <= targetAngle + 1f));
                    float calcSpeed = CalcSpeedRamp2(door.transform.localRotation.eulerAngles.y, startY,
                        startY + targetAngle);
                    door.transform.RotateAround(pivot.transform.position, Vector3.up, -1 * calcSpeed * Time.deltaTime);
                    //if (isTesting)
                    //Debug.Log("Close door Z: " + doorObj.transform.localRotation.eulerAngles.z.ToString("0.000"));
                    isClosing = true;
                    yield return null;
                }

                door.transform.localEulerAngles = new Vector3(door.transform.localEulerAngles.x,
                    door.transform.localEulerAngles.y, startY);
                door.transform.localPosition = startPos;
                break;
            case DoorType.Slidingv2:

                break;
        }


        isClosing = false;
    }

    private float CalcSpeedRamp(float value, float min, float max)
    {
        float curr = 1f;
        float percent = Mathf.Abs(value) / Mathf.Abs(max - min);

        if (percent < 0.3f) //30% speed up
            curr = percent * 10f * openSpeed;
        else if (percent > 0.7f) //70% slow down
            curr = (1f - percent) * 10f * openSpeed; //0.3, 0.2, 0.15, 0.1, 0
        else
            curr = openSpeed; //Regular Speed

        if (curr <= 5f) //1% speed safety
            curr = openSpeed * 0.01f;
        return curr;
    }

    private float CalcSpeedRamp2(float value, float min, float max)
    {
        float curr = 1f;
        float percent = Mathf.Abs(value) / Mathf.Abs(max - min);

        curr = openSpeed * speedCurve.Evaluate(percent);

        if (isClosing)
        {
            if (!playedCloseSound && 1f - percent >= playCloseSoundAtPercent)
            {
                //todo
                //  if (closeSound != null)
                //    audioSource.PlayOneShot(closeSound);
                if (isTesting)
                    Debug.Log("Play Close: Curr Precent: " + percent.ToString("0.000"));
                playedCloseSound = true;
            }
        }

        return curr;
    }

    enum DoorType
    {
        Normal,
        Sliding,
        Z,
        Slidingv2
    }

    public virtual void onInteract(Collider other)
    {
        TriggerDoor(other);
    }

    public virtual void onStopInteract(Collider other)
    {
         
    }
}

public interface Iinteractuable
{
    void onInteract(Collider other);
    void onStopInteract(Collider other);
}
