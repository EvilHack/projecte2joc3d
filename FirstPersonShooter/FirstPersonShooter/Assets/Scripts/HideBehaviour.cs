﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Collider)  )]
public class HideBehaviour : MonoBehaviour,Iinteractuable
{
    private bool isUsing = false;
    private Transform hidePosition;
    private Vector3 playerEnterPosition;
    private Transform player;
    // Start is called before the first frame update
    void Start()
    {
        hidePosition = this.transform.Find("HideTransform");
    }

    

    public void onInteract(Collider other)
    {
      CharacterController character=   other.GetComponent<CharacterController>();
        if (!isUsing)
        {
            player = other.transform;
            playerEnterPosition = other.transform.position; 
            character.enabled = false;
            //character.detectCollisions = false;
            other.transform.position = hidePosition.transform.position;
            Debug.Log("hidepos" + hidePosition.transform.position + "playerpos" + other.transform.position );
            isUsing = true;
            GameManager._instance.isPlayerHiding = true;
            character.enabled = false;
            GameManager._instance.UnlockAchievement("HideFirstTime");
        }
        else
        {
            
            other.transform.position = playerEnterPosition ;
            Debug.Log("backup pos" +  playerEnterPosition  + "playerpos" + other.transform.position);

            character.enabled = true;

            GameManager._instance.isPlayerHiding = false;          
            isUsing = false;
        }
    }

    public void onStopInteract(Collider other)
    {
       
    }
}
