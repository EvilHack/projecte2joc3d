﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorWithItemCondition : Door
{
    // Start is called before the first frame update
    [SerializeField] private Item necessaryItemToUnlock;
    [SerializeField] private bool specificQuantity;
    [SerializeField] private int quantity;
    private TextShowScript textShowScript;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public override void onInteract(Collider other)
    {
        FPOverwrite player = other.GetComponent<FPOverwrite>();
        bool condition = player.HasItemInTheInventory(necessaryItemToUnlock) &&
                         ((!specificQuantity) || (specificQuantity &&
                                                  quantity <= player.GetItemWithName(necessaryItemToUnlock.name)
                                                      .amount));
        if (condition)
        {
            ToggleDoor(id);
            textShowScript=   GetComponent<TextShowScript>();
            textShowScript.numText = 6;
            textShowScript.extraText = necessaryItemToUnlock.name;
            textShowScript.GuiOn=true;
            if (necessaryItemToUnlock.name != "Key") return;
            GameManager._instance.ChangeLevel(2);
            GameManager._instance.SaveIntoJson();
        }
        else
        {
            Debug.Log("Falta el item " + necessaryItemToUnlock.name);
           textShowScript=   GetComponent<TextShowScript>();
           textShowScript.numText = 5;
          textShowScript.extraText = necessaryItemToUnlock.name;
          textShowScript.GuiOn=true;
            
                ;
            ;
        }
    }

    public override void onStopInteract(Collider other)
    {
        base.onStopInteract(other);
        textShowScript.GuiOn = false;

    }
}